<?php


namespace Ucc\Controllers;

use Exception;
use Ucc\Services\QuestionService;
use Ucc\Session;
use Ucc\Http;

class QuestionsController
{
    use \Ucc\Http\JsonResponseTrait;

    private QuestionService $questionService;

    public function __construct(QuestionService $questionService)
    {
        $this->questionService = $questionService;
    }

    public function beginGame(): bool
    {
        $name = $this->getParameter('name');
        if (empty($name)) {
            return $this->json('You must provide a name', 400);
        }

        Session::set('name', $name);
        Session::set('questionCount', 1);
        //TODO Get first question for user
        $question = null;

        return    $this->json(['question' => $question], 201);
    }

    public function answerQuestion(int $id): bool {
        if ( Session::get('name') === null ) {
            return $this->json('You must first begin a game', 400);
        }

        if ((int)Session::get('questionCount') > 4) {
            $name = Session::get('name');
            $points = Session::get('points');
            Session::destroy();
            return $this->json(['message' => "Thank you for playing {$name}. Your total score was: {$points} points!"]);
        }

        $answer = $this->getParameter('answer');
        if (empty($answer)) {
            return $this->json('You must provide an answer', 400);
        }

        list($message, $question) = $this->getAnsware($id, $answer);

        $message = '';
        $question = null;

        return $this->json(['message' => $message, 'question' => $question]);
    }
    private function getAnsware(int $id, string $userAnswer): array
    {
        $answer = null;
        try {
            $questions = $this->loadFromFile($id);
            foreach ($questions AS $question)
            {
                if (isset($question['id']) && $question['id'] == $id) {
                    if ($question['correctAnswer'] == $userAnswer)
                    {
                        $possibleAnswers = rand(
                            1,
                            count($question['possibleAnswers'])
                        );
                        $answer = [
                            $question['possibleAnswers'][$possibleAnswers],
                            1 // return the user to the first answer
                        ];
                    }
                }
            }
            if ($answer === null)
            {
                $answer = [$questions['id'][1]];
            }
        } catch(Exception $expt) {
            $this->treatErrors($expt);
        }
        return $answer;
    }
    private function loadFromFile(int $id): array
    {
        $return = [];
        try {
            if (!$file = file_get_contents('./questions.json'))
            {
                throw new Exception('File not found', 0);
            }
            $return = (array) json_decode($file, true);
        } catch(Exception $expt) {
            $this->treatErrors($expt);
        }

        return $return;
    }
    private function getParameter(string $paramName): string
    {
        if (isset($_POST[$paramName])) {
            return $_POST[$paramName];
        } else {
            return null;
        }
    }
}
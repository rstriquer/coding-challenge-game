<?php


namespace Ucc\Http;

use Exception;

trait JsonResponseTrait
{
    public function json($data, int $statusCode = 200): bool
    {
        http_response_code($statusCode);
        header('Session-Id: ' . session_id());
        header('Content-Type: application/json;charset=utf-8');
        if (is_array($data)) {
            $body = '{"data": '.json_encode($data).'}';
        } else {
            $body = '{"data": {"response": '.json_encode($data).'}}';
        }
        if (json_last_error() !== JSON_ERROR_NONE) {
            $body = '{"error_code": "001", "error_message": "Undefined Error"}';
        }

        echo $body;
        exit();
    }

    /**
     * takes care that errors are handled correctly
     * 
     * @todo Improve the system of errors
     * @param Exception $expt
     * @author Ricado Soares <ricardo.soares@rentcars.com>
     * @throws Undocumented function
     */
    private function treatErrors(Exception $expt): void
    {
        // file_put_contents(
        //     '../logs/errors.log', 
        //     json_last_error_msg() . '(JSON ErrorId: '
        //         . json_last_error() . ')'
        //     FILE_APPEND
        // );
        echo 'Error Exception';
        exit(1);

    }
}